import React,{Component} from 'react';
import RouterIndex from './router'

// 创建并暴露app组件
export default class App extends Component{
  render(){
    return(
      <div>
        <RouterIndex/>
      </div>
    )

  }
}

import React from 'react';
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import List from '../../components/List'
import './index.css'
class Index extends React.Component{
  
	constructor(){
    super();
		this.state ={
      todos:[
        {id:'001',name:'吃饭',done:true},
        {id:'002',name:'睡觉',done:true},
        {id:'003',name:'打代码',done:false},
      ]
    }
  }

  // addTodo用于添加一个todos,接收的参数是todos对象
  addTodo = (todoObj)=>{
    // 获取原来的todos
    const { todos } = this.state
    // 追加一个todos
    const newTodos = [todoObj,...todos];
    // 更新状态
    this.setState({todos:newTodos})
  }

  // 修改todos
  updateTodo = (id,done)=>{
    // 获取状态中的todos
    const { todos } = this.state;
    // 加工数据
    const newTodos = todos.map((item,i)=>{
      if(item.id === id){
        return { ...item,done }
      }else{
        return item
      }
    })
    this.setState({
      todos:newTodos
    })
  }

  // 删除todos
  removeTodo = (id) =>{
    // 获取状态中的todos
    const { todos } = this.state;
    // todos.map((item,i)=>{
    //   if(item.id == id){
    //     todos.splice(i,1)
    //   }
    // })
    // this.setState({
    //   todos
    // })
    // 删除指定id的todo对象
    const newTodos = todos.filter((item,i)=>{
      return item.id !== id
    })
    this.setState({
      todos:newTodos
    })
  }


 // 全选、全不选
 handleChangdAllTodo = (done) =>{
  // 获取状态中的todos
  const { todos } = this.state;
  // 全选todo对象
  const newTodos = todos.map((item,i)=>{
    return {...item,done}
  })
  this.setState({
    todos:newTodos
  })
 }

 //  删除已选择的todo
  hanldeRemoveAllTodo = () =>{
    // 获取状态中的todos
    const { todos } = this.state;
    const newTodos = todos.filter(item=>{
      return !item.done
    })
    this.setState({
      todos:newTodos
    })
  }

  render(){
    const {
      todos
    } = this.state
		return (
      <div className="todo-container">
        <div className="todo-wrap">
          <Header addTodo={this.addTodo}/>
          <List 
             todos={todos}
             updateTodo={this.updateTodo} 
             removeTodo={this.removeTodo}
          />
          <Footer 
             todos={todos}
             handleChangdAllTodo = {this.handleChangdAllTodo}
             hanldeRemoveAllTodo = {this.hanldeRemoveAllTodo}
          />
          
        </div>
      </div>
		)
	}
}

export default Index


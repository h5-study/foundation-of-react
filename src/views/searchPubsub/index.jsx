import React, { Component } from 'react'
import Searchs from '../../components/Searchs'
import SearchLists from '../../components/SearchLists'
import img1 from '../../img/1.jpeg'
import './index.css'

export default class SearchPubsub extends Component {
  render() {
    return (
      <div className="container">
        <img src={img1} alt="" />
        <Searchs/>
        <SearchLists/>
      </div>
    )
  }
}

import React, { Component } from 'react'
import MyNavLink from '../../components/MyNavLink'
import {BrowserRouter,Route,Switch,Redirect} from 'react-router-dom'
import News from './Home/News'
import Message from './Home/Message'

export default class Home extends Component {
	render() {
		return (
			<BrowserRouter>
				<div>
					<h3>我是Home的内容</h3>
					<div>
						<ul className="nav nav-tabs">
							<li>
								<MyNavLink to="/studyTwoRoute/news">News</MyNavLink>
							</li>
							<li>
								<MyNavLink to="/studyTwoRoute/message">Message</MyNavLink>
							</li>
						</ul>
						{/* 注册路由 */}
						<Switch>
							<Route path="/studyTwoRoute/news" component={News}/>
							<Route path="/studyTwoRoute/message" component={Message}/>
							<Redirect to="/studyTwoRoute/news"/>
						</Switch>
					</div>
				</div>
			</BrowserRouter>
			)
	}
}

import React, { Component } from 'react'
import { BrowserRouter, Route ,Redirect} from 'react-router-dom'
import Header from '../../components/Header'
import Search from '../../components/Search'
import MyNavLink from '../../components/MyNavLink'
import './index.css'

export default class stydyRoute extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          {/* <NavLink activeClassName="isactive" to="/Header">Header</NavLink>
          <NavLink activeClassName="isactive" to="/Search">Search</NavLink> */}

          <MyNavLink to="/Header">Header11</MyNavLink>
          <MyNavLink to="/Search">Search11</MyNavLink>


          <Route  path="/Header" component={Header} />
          <Route  path="/Search" component={Search} />
          <Redirect to='/Header'/>
        </BrowserRouter>
      </div>
    )
  }
}

import React, { Component } from 'react'
import Searchs from '../../components/Search'
import SearchList from '../../components/SearchList'
import './index.css'

export default class Search extends Component {
  state = {
    userList : [],
    isFirst:true,   //是否为第一次打开页面
    isLoading:false,   //标识是否处于加载中
    err:'',   //存储请求相关的错误信息
  }

  updateSearchState = (stateObj)=>{
    this.setState(stateObj)
    console.log(this.state)
  }

  render() {
    return (
      <div className="container">
        <Searchs  
          updateSearchState={this.updateSearchState} 
        />
        <SearchList {...this.state}/>
      </div>
    )
  }
}

import React, { Component } from 'react'

export default class SearchList extends Component {



  render() {
    const { userList , isFirst,isLoading,err } = this.props
    return (
      <div className="row">
        {
          isFirst ? <h2>欢迎使用，输入关键字，随后点击搜索</h2>:
          isLoading ? <h2>isLoading...</h2>:
          err ? <h2>{err}</h2>:
          userList.map((item, i) => {
            return (
              <div className="card" key={i}>
                <a rel="noreferrer" href={item.html_url} target="_blank">
                  <img alt="" src={item.avatar_url} style={{ width: 100 + 'px' }} />
                </a>
                <p className="card-text">{item.login}</p>
              </div>
            )
          })
        }

      </div>
    )
  }
}

import React, { Component } from 'react'
import pubSub from 'pubsub-js'   //订阅函数

export default class SearchLists extends Component {

  state = {
    userList : [],
    isFirst:true,   //是否为第一次打开页面
    isLoading:false,   //标识是否处于加载中
    err:'',   //存储请求相关的错误信息
  }

  componentDidMount(){
    // 订阅消息
    this.token = pubSub.subscribe('atguigu',(msg,data)=>{
      this.state(data)
    })
  }

  componentWillUnmount(){
    // 页面关闭取消订阅消息
    pubSub.unsubscribe(this.token)
  }



  render() {
    const { userList , isFirst,isLoading,err } = this.state
    return (
      <div className="row">
        {
          isFirst ? <h2>欢迎使用，输入关键字，随后点击搜索</h2>:
          isLoading ? <h2>isLoading...</h2>:
          err ? <h2>{err}</h2>:
          userList.map((item, i) => {
            return (
              <div className="card" key={i}>
                <a rel="noreferrer" href={item.html_url} target="_blank">
                  <img alt="" src={item.avatar_url} style={{ width: 100 + 'px' }} />
                </a>
                <p className="card-text">{item.login}</p>
              </div>
            )
          })
        }

      </div>
    )
  }
}

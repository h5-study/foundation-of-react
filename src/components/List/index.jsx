import React, { Component } from 'react'
import propTypes from 'prop-types'     
import Item from '../Item'
import './index.css'

export default class List extends Component {

  // 对接收的props进行：类型、必要性的限制
  static propTypes = {
    todos:propTypes.array.isRequired,
    updateTodo:propTypes.func.isRequired,
    removeTodo:propTypes.func.isRequired,
  }


  render() {
    const { todos ,updateTodo,removeTodo} = this.props
    return (
      <ul className="todo-main">
        {
          todos.map((item,i)=>{
            return <Item 
              key={i}
              {...item}
              updateTodo={updateTodo}
              removeTodo={removeTodo}
            />
          })
        }
      </ul>
    )
  }
}

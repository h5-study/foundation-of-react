import axios from 'axios'
import React, { Component } from 'react'

export default class Searchs extends Component {

  constructor(){
    super()
    this.refName = React.createRef()
  }

  getSearchValue =()=>{
    this.props.updateSearchState({isFirst:false,isLoading:true})

    axios.get(`http://localhost:3000/api1/searcqqh/users?q=${this.refName.current.value}`).then((res)=>{
        this.props.updateSearchState({userList:res.data.items,isLoading:false})
      })
      .catch((err)=>{
        this.props.updateSearchState({err:err.message,isLoading:false})
      })
  }



  render() {
    return (
      <div>
        <section className="jumbotron">
          <h3 className="jumbotron-heading">Search Github Users</h3>
          <div>
            <input type="text" ref={ this.refName } placeholder="enter the name you search" />
            &nbsp;<button onClick={this.getSearchValue.bind(this)}>Search</button>
          </div>
        </section>
      </div>
    )
  }
}

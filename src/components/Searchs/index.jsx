import axios from 'axios'
import pubSub from 'pubsub-js'   //订阅函数
import React, { Component } from 'react'

export default class Searchs extends Component {

  constructor(){
    super()
    this.refName = React.createRef()
  }




  getSearchValue =()=>{
    // 发布消息
    pubSub.publish('atguigu',{isFirst:false,isLoading:true})
    axios.get(`http://localhost:3000/api1/searcqqh/users?q=${this.refName.current.value}`).then((res)=>{
        pubSub.publish('atguigu',{userList:res.data.items,isLoading:false})
      })
      .catch((err)=>{
        pubSub.publish('atguigu',{err:err.message,isLoading:false})
      })
  }



  render() {
    return (
      <div>
        <section className="jumbotron">
          <h3 className="jumbotron-heading">Search Github Users</h3>
          <div>
            <input type="text" ref={ this.refName } placeholder="enter the name you search" />
            &nbsp;<button onClick={this.getSearchValue.bind(this)}>Search</button>
          </div>
        </section>
      </div>
    )
  }
}

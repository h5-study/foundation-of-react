import React, { Component } from 'react'

export default class Footer extends Component {

  // 全选、全不选
  hanldeChangeAll = (e) =>{
    this.props.handleChangdAllTodo(e.target.checked)
  }
  // 清楚已完成任务
  hanldeRemoveAll = () =>{
    this.props.hanldeRemoveAllTodo()
  }

  render() {
    const { todos } = this.props
    // 已完成的个数
    const doneCount = todos.reduce((pre,todo)=> pre + ( todo.done ? 1 : 0 ),0)
    // 总数
    const total = todos.length
 
    return (
      <div className="todo-footer" >
        <label>
          <input type="checkbox" 
            checked={doneCount === total && total !== 0 ? true : false}
            onChange={this.hanldeChangeAll.bind(this)}  
          />
        </label>
        <span>
          <span>已完成{doneCount}</span> / 全部{total}
            </span>
        <button onClick={this.hanldeRemoveAll} className="btn btn-danger">清除已完成任务</button>
      </div>
    )
  }
}

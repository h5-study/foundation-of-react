import React, { Component } from 'react'
import propTypes from 'prop-types'     
import {nanoid} from 'nanoid'   //可以生产唯一Id
import './index.css'

export default class Header extends Component {

  // 对接收的props进行：类型、必要性的限制
  static propTypes = {
    addTodo:propTypes.func.isRequired
  }

  // 键盘按下事件
  handleKeyUp = (event)=>{
    // 解构赋值获取target , keyCode
    const {target , keyCode} = event;
    // 判读是否按下回车键
    if(keyCode === 13){
      // 添加的todo名字不能为空
      if(target.value.trim() === ''){
        alert('输入不能为空')
        return;
      }
      // 准备好一个todo对象
      const todoObj = {id:nanoid(),name:target.value,done:false}
      // 向父组件传值
      this.props.addTodo(todoObj)
      // 清空输入框
      target.value = ""
    }
  }


  render() {
    return (
      <div>
        <div className="todo-header">
          <input onKeyUp={this.handleKeyUp} type="text" placeholder="请输入你的任务名称，按回车键确认"/>
        </div>
      </div>
    )
  }
}

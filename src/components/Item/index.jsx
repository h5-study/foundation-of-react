import React, { Component } from 'react'
import './index.css'
export default class Item extends Component {

		state = {
      mouse:false
    }
  
  // 鼠标移入移出的回调
  hanldeMouse = (flag)=>{
    this.setState({
      mouse:flag
    })
  }
  // 勾选、取消勾选某一个todo的回调
  hanldeChange = (id,e)=>{
    this.props.updateTodo(id,e.target.checked)
  }
  // 删除一个todo的回调
  hanldeRemove = (id)=>{
    this.props.removeTodo(id)
  }


  render() {
    const { id,name,done } = this.props
    const { mouse } = this.state
    return (
      <div>
        <li style={{background:mouse?"#ccc":''}} onMouseEnter={this.hanldeMouse.bind(this,true)} onMouseLeave={this.hanldeMouse.bind(this,false)}>
          <label>
            <input type="checkbox" checked={done} onChange={this.hanldeChange.bind(this,id)}/>
            <span>{name}</span>
          </label>
          <button onClick={this.hanldeRemove.bind(this,id)} className="btn btn-danger" style={{ display: mouse ? 'block' :'none' }}>删除</button>
        </li>
      </div>
    )
  }
}

import React, { Component } from "react";
import {  Switch, Route, Router } from "react-router-dom";

import Search  from '../views/search';
import SearchPubsub  from '../views/searchPubsub';
import Index  from '../views/index';
import StudyRoute  from '../views/studyRoute';
import StudyTwoRoute  from '../views/studyTwoRoute';


// 改成history模式，需要下载history插件---需要放置在引入文件下面
import {createBrowserHistory} from 'history'
const history = createBrowserHistory()

class RouterIndex extends Component {
  render() {
    return (
      <Router history={history} >
        <Switch>
          <Route exact path="/" component={Index} />
          <Route exact path="/search" component={Search} />
          <Route exact path="/searchPubsub" component={SearchPubsub} />
          <Route exact path="/studyRoute" component={StudyRoute} />
          <Route exact path="/studyTwoRoute" component={StudyTwoRoute} />
        </Switch>
      </Router>
    );
  }
}

export default RouterIndex;